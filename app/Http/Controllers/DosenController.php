<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DosenController extends Controller
{
    public function index(){
        $nama = "Muhammad Ghazali Suwardi";
        $huruf = ["a","b","c","d"];
        return view('biodata',['namaku'=>$nama,'hurufku'=>$huruf]);
    }
}
